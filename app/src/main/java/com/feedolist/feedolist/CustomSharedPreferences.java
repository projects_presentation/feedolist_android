package com.feedolist.feedolist;

import android.content.Context;
import android.content.SharedPreferences;

public class CustomSharedPreferences {

    private static final String SHARED_PREFERENCES_FILE = "sharedPreferences";

    private static final String HTTP_HTTPS = "https://";
    //private static final String SERVER_ADDRESS = "192.168.1.71";
    private static final String SERVER_ADDRESS = "feedolist.ddns.net";
    private static final int SERVER_PORT = 443;

    public static void createSharedPreferences(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("SERVER_ADDRESS",HTTP_HTTPS + SERVER_ADDRESS + ":" + SERVER_PORT + "/");
        editor.commit();
    }

    public static void storeString(String key, String stringToStore, Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key,stringToStore);
        editor.commit();
    }

    public static String getString(String key, Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key,"");
    }

}
