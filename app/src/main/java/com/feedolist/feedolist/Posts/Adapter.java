package com.feedolist.feedolist.Posts;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;

import com.feedolist.feedolist.Extras.CustomProgressDialog;
import com.feedolist.feedolist.Extras.Utilities;
import com.feedolist.feedolist.R;
import com.feedolist.feedolist.ServerRequests.GetPosts;
import com.feedolist.feedolist.ServerRequests.Post;
import com.feedolist.feedolist.ServerRequests.User;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements GetPosts.IRequest{

    private GetPosts getPosts;

    private ArrayList<Post> postsBuffer = new ArrayList<>();

    private int queueLength = 0;

    private Context context;
    private CustomProgressDialog customProgressDialog;

    private static final int POST_ITEM_VIEW_TYPE = 0;
    private static final int AD_VIEW_TYPE = 1;
    private static final int POSTS_BEFORE_ADS = 4;

    public Adapter(Context context){
        this.context = context;
        getPosts = new GetPosts(context);
        customProgressDialog = new CustomProgressDialog(context, context.getString(R.string.getting_posts));
        startBackupProcesses();
    }

    private void startBackupProcesses(){
        getPosts.setOnRequestResponse(this);
    }

    public void updateList(ArrayList<String> usernames){
        customProgressDialog.showProgressDialog();
        postsBuffer.clear();
        queueLength = usernames.size();
        for (String username : usernames
             ) {
            getPosts.callRequest(username);
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View view;
        switch (viewType){
            case AD_VIEW_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.native_ad_view_holder,parent,false);
                viewHolder = new NativeAdViewHolder(view);
                break;
            case POST_ITEM_VIEW_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_data_row,parent,false);
                viewHolder = new ViewHolder(view);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {



        if(holder instanceof ViewHolder){
            Post post = (Post) postsBuffer.get(position);
            ViewHolder viewHolder = (ViewHolder) holder;
            ((ViewHolder) holder).SetPost(post,context);
            SpannableStringBuilder postClickable = Utilities.addClickableSpan("Likes: " + post.getLikes() + "\n" + post.getText());
        }

        setAnimation(holder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return postsBuffer.size();
    }

    @Override
    public int getItemViewType(int position){
        if(position != 0 && position % POSTS_BEFORE_ADS == 0 ){
            return AD_VIEW_TYPE;
        }
        else {
            return POST_ITEM_VIEW_TYPE;
        }
    }

    @Override
    public void onRequestResponse(User user, ArrayList<Post> posts) {
        if(posts != null) {
            postsBuffer.addAll(posts);
            Collections.sort(postsBuffer);
        }
        if(--queueLength == 0){
            customProgressDialog.hideProgressDialog();
            notifyDataSetChanged();
        }
    }

    private int lastPosition = -1;

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(new Random().nextInt(501));//to make duration random number between [0,501)
            viewToAnimate.startAnimation(anim);
            lastPosition = position;
        }
    }

    //region OnClickListener
    private OnClickListener onClickListener;

    public interface OnClickListener{
        void onClick(String username);
    }

    public void setOnClickListener(OnClickListener onClickListener){
        this.onClickListener = onClickListener;
    }
    //endregion

}
