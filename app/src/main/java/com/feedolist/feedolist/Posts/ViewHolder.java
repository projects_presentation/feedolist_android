package com.feedolist.feedolist.Posts;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.feedolist.feedolist.R;
import com.feedolist.feedolist.ServerRequests.Post;
import com.squareup.picasso.Picasso;

public class ViewHolder extends RecyclerView.ViewHolder {

    private ImageView imageView;
    private TextView username;
    private TextView postText;

    private Post post;

    public ViewHolder(View itemView) {
        super(itemView);
        imageView = itemView.findViewById(R.id.post_image);
        username = itemView.findViewById(R.id.post_username);
        postText = itemView.findViewById(R.id.post_data);
    }

    public void SetPost(Post post, Context context){
        username.setText(post.getUsername());
        this.postText.setText(post.getText());
        this.postText.setMovementMethod(LinkMovementMethod.getInstance());
        Picasso.get().load(post.getImageUrl()).into(imageView);
    }
}
