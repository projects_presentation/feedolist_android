package com.feedolist.feedolist.Posts;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;

import com.feedolist.feedolist.Instagram.InsertInstagramUserAfterClick;
import com.feedolist.feedolist.R;

import java.util.ArrayList;

public class ListFragment extends Fragment implements Adapter.OnClickListener {

    private OnFragmentInteractionListener mListener;
    private static final String usernamesKey = "USERNAMES";
    private static final String listNameKey = "LISTNAME";

    private Adapter adapter = null;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;

    private String _listName = "";

    private OnClickListener onClickListener;

    public interface OnClickListener{
        void onClick(String username);
    }

    public void setOnClickListener(OnClickListener onClickListener){
        this.onClickListener = onClickListener;
    }

    public static ListFragment newInstance(String listName, ArrayList<String> usernames) {
        ListFragment fragment = new ListFragment();
        Bundle args = new Bundle();
        args.putString(listNameKey,listName);
        args.putStringArrayList(usernamesKey, usernames);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_fragment_layout, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        showAds(view);

        TextView textView = view.findViewById(R.id.list_name_tv);
        textView.setText(_listName);

        layoutManager = new LinearLayoutManager(getContext());
        adapter = new Adapter(getContext());

        recyclerView = view.findViewById(R.id.content_recycler_view);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        adapter.setOnClickListener(this);

        if (getArguments() != null) {
            _listName = getArguments().getString(listNameKey);
            ArrayList<String> usernamesList = getArguments().getStringArrayList(usernamesKey);
            adapter.updateList(usernamesList);
        }
    }


    private void showAds(View view){
        AdView adView = new AdView(getActivity(), "331168737350651_436165503517640", AdSize.BANNER_HEIGHT_50);
        adView.setAdListener(new AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Ad loaded callback
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
            }
        });
        LinearLayout adContainer = view.findViewById(R.id.bottomBannerAds);
        adContainer.addView(adView);
        adView.loadAd();
    }

    @Override
    public void onClick(String username) {
        new InsertInstagramUserAfterClick(getContext()).callRequest(username);
        onClickListener.onClick(username);
    }

    //region Required Methods

    public ListFragment() {
        // Required empty public constructor
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSaveInstanceState( Bundle outState ) {

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    //endregion
}
