package com.feedolist.feedolist.Posts;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdListener;
import com.facebook.ads.NativeAdView;
import com.feedolist.feedolist.R;

public class NativeAdViewHolder extends RecyclerView.ViewHolder {

    private NativeAd nativeAd;

    public NativeAdViewHolder(View itemView) {
        super(itemView);
        requestAd(itemView);
    }

    private void requestAd(final View itemView){
        // Instantiate an NativeAd object.
        // NOTE: the placement ID will eventually identify this as your App, you can ignore it for
        // now, while you are testing and replace it later when you have signed up.
        // While you are using this temporary code you will only get test ads and if you release
        // your code like this to the Google Play your users will not receive ads (you will get a no fill error).
        nativeAd = new NativeAd(itemView.getContext(), "331168737350651_437395463394644");
        nativeAd.setAdListener(new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {

            }

            @Override
            public void onError(Ad ad, AdError adError) {

            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Render the Native Ad Template
                View adView = NativeAdView.render(itemView.getContext(), nativeAd, NativeAdView.Type.HEIGHT_300);
                LinearLayout nativeAdContainer = itemView.findViewById(R.id.nativeAdLayout);
                // Add the Native Ad View to your ad container
                nativeAdContainer.addView(adView);
            }


            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        });

        // Initiate a request to load an ad.
        nativeAd.loadAd();
    }
}
