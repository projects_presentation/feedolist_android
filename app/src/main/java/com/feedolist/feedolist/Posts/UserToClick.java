package com.feedolist.feedolist.Posts;

/**
 * Created by jose on 31/10/2017.
 */

public class UserToClick {

    public String username;
    public int startIndex;
    public int endIndex;

    public UserToClick(String username,int startIndex,int endIndex){
        if(username.endsWith(".")) username = username.substring(0,username.length()-1);
        this.username =username.trim();
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }

    @Override
    public String toString(){
        return "Username: " + username + " startIndex: " + startIndex + " endIndex" + endIndex;
    }
}
