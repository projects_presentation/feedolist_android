package com.feedolist.feedolist.PersonalLists;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListPopupWindow;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.feedolist.feedolist.R;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class PersonalListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public enum ClickType{
        LIST_CLICKED,
        ADD_USER_CLICKED,
        EDIT_CLICKED,
        DELETE_CLICKED
    }

    private TextView listName;
    private ImageView listMenu;
    private PersonalList personalList;

    public PersonalListViewHolder(View itemView) {
        super(itemView);
        listName = (TextView) itemView.findViewById(R.id.user_list_name);
        listMenu = (ImageView) itemView.findViewById(R.id.menu_image_btn);
        listMenu.setOnClickListener(this);
        itemView.setOnClickListener(this);
        listMenu.setOnClickListener(this);
    }

    private OnClickListener onClickListener;

    public interface OnClickListener{
        void onClick(PersonalList personalList, ClickType clickType);
    }

    public void setOnClickListener(OnClickListener onClickListener){
        this.onClickListener = onClickListener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            default:
                onClickListener.onClick(personalList,ClickType.LIST_CLICKED);
                break;
            case R.id.menu_image_btn:
                PopupMenu popupMenu = new PopupMenu(v.getContext(),v);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.add_users_to_list_menu_btn:
                                onClickListener.onClick(personalList,ClickType.ADD_USER_CLICKED);
                                break;
                            case R.id.edit_list_name_menu_btn:
                                onClickListener.onClick(personalList,ClickType.EDIT_CLICKED);
                                break;
                            case R.id.delete_list_menu_btn:
                                onClickListener.onClick(personalList,ClickType.DELETE_CLICKED);
                                break;
                        }
                        return false;
                    }
                });
                setForceShowIcon(popupMenu);
                popupMenu.inflate(R.menu.edit_list_menu);
                popupMenu.show();
                break;
        }
    }

    public static void setForceShowIcon(PopupMenu popupMenu) {
        try {
            Field[] fields = popupMenu.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popupMenu);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper
                            .getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod(
                            "setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void setPersonalList(PersonalList personalList){
        this.personalList = personalList;
        listName.setText("#"+ personalList.getListName());
    }
}
