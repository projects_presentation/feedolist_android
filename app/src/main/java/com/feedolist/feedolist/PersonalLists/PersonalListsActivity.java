package com.feedolist.feedolist.PersonalLists;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.feedolist.feedolist.PersonalUsers.PersonalUsersActivity;
import com.feedolist.feedolist.R;

import static com.feedolist.feedolist.MainActivity.USERS_SELECTION_REQUEST;

public class PersonalListsActivity extends AppCompatActivity implements View.OnClickListener, PersonalListsAdapter.OnClickListener {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private PersonalListsAdapter personalListsAdapter;

    private Button doneButton;
    private Button createNewListButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startUserInterface();
    }


    private void startUserInterface(){

        setContentView(R.layout.personal_lists_activity_layout);


        doneButton = (Button) findViewById(R.id.done_editing_lists_btn);
        doneButton.setOnClickListener(this);
        createNewListButton = (Button) findViewById(R.id.create_new_list_btn);
        createNewListButton.setOnClickListener(this);
        recyclerView = (RecyclerView) findViewById(R.id.personal_users_recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        ViewGroup viewGroup = (ViewGroup) findViewById(android.R.id.content);
        personalListsAdapter = new PersonalListsAdapter(this,viewGroup);
        personalListsAdapter.setOnClickListener(this);
        recyclerView.setAdapter(personalListsAdapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.done_editing_lists_btn:
                finish();
                break;
            case R.id.create_new_list_btn:
                personalListsAdapter.createNewList();
                break;
        }
    }

    @Override
    public void onClick(PersonalList personalList) {
        Intent intent = new Intent(PersonalListsActivity.this,PersonalUsersActivity.class);
        intent.putExtra("personalList",personalList);
        startActivityForResult(intent, USERS_SELECTION_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == USERS_SELECTION_REQUEST) {
            if (resultCode == RESULT_OK) {
                setResult(resultCode,data);
                finish();
            }
        }
    }

}
