package com.feedolist.feedolist.PersonalLists;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.feedolist.feedolist.FirebaseDatabase.FirebaseManager;
import com.feedolist.feedolist.R;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class PersonalListsAdapter extends RecyclerView.Adapter<PersonalListViewHolder> implements FirebaseManager.IFirebaseRequest, PersonalListViewHolder.OnClickListener{

    private ArrayList<PersonalList> personalLists = new ArrayList<>();
    private FirebaseManager firebaseManager = null;
    private Context context = null;
    private ViewGroup viewGroup;

    public PersonalListsAdapter(Context context, ViewGroup viewGroup){
        this.context = context;
        this.viewGroup = viewGroup;
        firebaseManager = new FirebaseManager(context);
        firebaseManager.setOnFirebaseResponse(this);
        firebaseManager.getUserLists();
    }

    private OnClickListener onClickListener;

    public interface OnClickListener{
        void onClick(PersonalList personalList);

    }

   public void setOnClickListener(OnClickListener onClickListener){
        this.onClickListener = onClickListener;
    }

    public void createNewList(){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getResources().getString(R.string.create_list));
        builder.setPositiveButton("Ok",null);
        builder.setNegativeButton("Cancel",null);
        View viewInflated = LayoutInflater.from(context).inflate(R.layout.create_list_dialog_layout,viewGroup , false);
        final EditText input = (EditText) viewInflated.findViewById(R.id.input_list_name);
        builder.setView(viewInflated);

        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button button = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String newListName = input.getText().toString();
                        if(newListName.equals("") || newListName.length() > 20){
                            Toast.makeText(context, "List name cannot be empty and cannot contain more than 20 characters!", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(context, "New list: " + newListName, Toast.LENGTH_SHORT).show();
                            alertDialog.dismiss();
                            firebaseManager.createList(new PersonalList(newListName));
                            firebaseManager.getUserLists();
                        }
                    }
                });
            }
        });

        alertDialog.show();
    }

    public void deleteList(final PersonalList personalList){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Delete: " + personalList.getListName() + "\nAre you sure?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                firebaseManager.deleteList(personalList);
                firebaseManager.getUserLists();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    public void editListName(final PersonalList personalList){

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Edit list name");
        builder.setPositiveButton("Ok",null);
        builder.setNegativeButton("Cancel",null);
        View viewInflated = LayoutInflater.from(context).inflate(R.layout.create_list_dialog_layout,viewGroup , false);
        final EditText input = (EditText) viewInflated.findViewById(R.id.input_list_name);
        input.setText(personalList.getListName());
        builder.setView(viewInflated);

        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button button = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String newListName = input.getText().toString();
                        if(newListName.equals("") || newListName.length() > 20){
                            Toast.makeText(context, "List name cannot be empty and cannot contain more than 20 characters!", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            alertDialog.dismiss();
                            firebaseManager.updateListName(personalList.getKey(),newListName);
                            firebaseManager.getUserLists();
                        }
                    }
                });
            }
        });

        alertDialog.show();
    }

    @Override
    public PersonalListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.personal_list_item_layout,parent,false);
        PersonalListViewHolder viewHolder = new PersonalListViewHolder(view);
        viewHolder.setOnClickListener(this);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PersonalListViewHolder holder, int position) {
        holder.setPersonalList(personalLists.get(position));
    }

    @Override
    public int getItemCount() {
        return personalLists.size();
    }

    @Override
    public void onFirebaseResponse(ArrayList<Object> list, FirebaseManager.ResponseType responseType) {
        if(responseType == FirebaseManager.ResponseType.UserLists){
            personalLists = (ArrayList<PersonalList>)(ArrayList<?>)(list);
            notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(PersonalList personalList, PersonalListViewHolder.ClickType clickType) {
        switch (clickType){
            case LIST_CLICKED:
                onClickListener.onClick(personalList);
                break;
            case ADD_USER_CLICKED:
                onClickListener.onClick(personalList);
                break;
            case EDIT_CLICKED:
                editListName(personalList);
                break;
            case DELETE_CLICKED:
                deleteList(personalList);
                break;
        }

    }
}
