package com.feedolist.feedolist.PersonalLists;

import com.feedolist.feedolist.ServerRequests.User;

import java.io.Serializable;
import java.util.ArrayList;

public class PersonalList implements Serializable{

    private String key;
    private String listName;
    private ArrayList<User> usersList = new ArrayList<>();

    public PersonalList(String listName){
        this.listName = listName;
    }

    public PersonalList(String key, String listName){
        this.key = key;
        this.listName = listName;
    }

    public String getListName() {
        return listName;
    }

    public void setUsersList(ArrayList<User> usersList){

        this.usersList = usersList;
    }

    public ArrayList<User> getUsersList() {
        return usersList;
    }

    public String getKey() {
        return key;
    }
}
