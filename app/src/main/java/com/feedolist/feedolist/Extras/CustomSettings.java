package com.feedolist.feedolist.Extras;

public class CustomSettings {

    public static final Boolean DEBUG = true;

    private final static String SERVER_ADDRESS_DEBUG = "http://Joses-MBP";
    private final static String SERVER_ADDRESS_RELEASE = "http://Joses-MBP";
    private final static int SERVER_PORT_DEBUG = 3000;
    private final static int SERVER_PORT_RELEASE = 3000;



    public static String getServerAddress() {
        return DEBUG ? SERVER_ADDRESS_DEBUG : SERVER_ADDRESS_RELEASE;
    }

    public static int getServerPort() {
        return DEBUG ? SERVER_PORT_DEBUG : SERVER_PORT_RELEASE;
    }
}
