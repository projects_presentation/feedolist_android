package com.feedolist.feedolist.Extras;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;

import com.feedolist.feedolist.Posts.UserToClick;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by jose on 14/05/2017.
 */

public class Utilities {

    private static int PERMISSION_ALL = 1;

    private static final String[] permissions = {Manifest.permission.INTERNET};

    private static boolean hasPermissions(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void requestPermissions(Context context) {
        if (!hasPermissions(context)) {
            ActivityCompat.requestPermissions((Activity) context, permissions, PERMISSION_ALL);
        }
    }


    public final static boolean isValidEmail(CharSequence target) {
        return !(target == null) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private final static String DEBUG_TAG = "MY-SHIFTS-DEBUG";

    public final static void logToConsole(String message) {
        Log.d(DEBUG_TAG, message);
    }

    public final static void logToConsole(Exception exception) {
        if (exception.getMessage() != null) {
            Log.d(DEBUG_TAG, exception.getMessage());
        }
    }

    public static void logMessage(String message) {
        Log.d("PVMDebug", message);
    }

    public static Date convertDate(String dateString) {
        Date parsed = new Date();
        try {
            //2017-06-19T15:55:55.000Z
            SimpleDateFormat format =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            parsed = format.parse(dateString);
        } catch (ParseException pe) {
            return null;
        }
        return parsed;
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean iiLetterOrDigit(char c){
        if(c == '_'){
            return true;
        }
        else if(c == '.'){
            return true;
        }
        else {
            return Character.isLetterOrDigit(c);
        }
    }

    public static SpannableStringBuilder addClickableSpan(String postText){

        int atIndex = 0;
        int spaceIndex = 0;
        String username = "";
        ArrayList<UserToClick> listOfUsersToClick = new ArrayList<>();
        int lastIndex = 0;

        for(int i=0;i< postText.length();++i){
            lastIndex = i;

            if(postText.charAt(i) == '@'){
                atIndex = i;
            }

            else if(!iiLetterOrDigit(postText.charAt(i)) && atIndex != 0 ){
                spaceIndex = i;
            }

            else if(atIndex != 0){
                username += postText.charAt(i);
            }
            if(atIndex != 0 && spaceIndex != 0){
                UserToClick userToClick = new UserToClick(username,atIndex,spaceIndex);
                listOfUsersToClick.add(userToClick);
                atIndex = 0;
                spaceIndex = 0;
                username = "";
            }
        }

        if(atIndex != 0){
            UserToClick userToClick = new UserToClick(username,atIndex,lastIndex+1);
            listOfUsersToClick.add(userToClick);
        }


        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(postText);
        ClickableSpan clickableSpan;

        for (final UserToClick userToClick: listOfUsersToClick) {
            clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    //onClickListener.onClick(userToClick.username);
                }
            };

            ssBuilder.setSpan(
                    clickableSpan,
                    userToClick.startIndex,
                    userToClick.endIndex,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            );
        }

        return ssBuilder;
    }
}
