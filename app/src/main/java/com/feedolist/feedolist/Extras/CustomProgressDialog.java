package com.feedolist.feedolist.Extras;

import android.app.ProgressDialog;
import android.content.Context;

public class CustomProgressDialog {
    private Context context;
    private String message;
    private ProgressDialog mProgressDialog;

    public CustomProgressDialog(Context context, String message) {
        this.context = context;
        this.message = message;
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage(message);
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
