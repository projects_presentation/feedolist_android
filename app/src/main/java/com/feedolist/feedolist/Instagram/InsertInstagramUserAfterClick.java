package com.feedolist.feedolist.Instagram;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by jose on 02/11/2017.
 */

public class InsertInstagramUserAfterClick implements Response.Listener, Response.ErrorListener{
    private Context context;

    public InsertInstagramUserAfterClick(Context context) {
        this.context = context;
    }

    public void callRequest(String username) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = "https://feedolist.herokuapp.com/instagram/insert_user_mobile/?username="+username;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, this, this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    @Override
    public void onResponse(Object response) {
        try {
            JSONObject jsonObject = new JSONObject(response.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        error.printStackTrace();
    }
}
