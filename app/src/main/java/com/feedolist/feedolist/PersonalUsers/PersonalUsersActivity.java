package com.feedolist.feedolist.PersonalUsers;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.feedolist.feedolist.PersonalLists.PersonalList;
import com.feedolist.feedolist.R;
import com.feedolist.feedolist.ServerRequests.UpdateUserPosts;
import com.feedolist.feedolist.ServerRequests.User;

import java.util.ArrayList;

public class PersonalUsersActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;

    private TextView title;
    private Button viewListButton;
    private Button addUserButton;

    private PersonalList personalList;

    private PersonalUsersAdapter personalUsersAdapter;

    public static int USER_CREATION_RESULT = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDataToStartActivity();
        startUserInterface();
    }

    private void startUserInterface() {
        setContentView(R.layout.personal_users_activity_layout);

        title = (TextView) findViewById(R.id.personal_users_title);
        title.setText("#" + personalList.getListName().replaceAll(" ", "").toLowerCase());

        viewListButton = (Button) findViewById(R.id.personal_users_view_all_btn);
        viewListButton.setOnClickListener(this);

        addUserButton = (Button) findViewById(R.id.personal_users_add_user_btn);
        addUserButton.setOnClickListener(this);

        personalUsersAdapter = new PersonalUsersAdapter(personalList, this);

        recyclerView = (RecyclerView) findViewById(R.id.personal_users_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(personalUsersAdapter);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

    }

    private void getDataToStartActivity() {
        Bundle bundle = getIntent().getExtras();
        personalList = (PersonalList) bundle.get("personalList");
    }

    private void returnResult(){
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putString("selectionText","#" + personalList.getListName().replace(" ","").toLowerCase());

        ArrayList<String> selectedUsers = new ArrayList<>();
        for (User user: personalUsersAdapter.getPersonalList().getUsersList()) {
            selectedUsers.add(user.getUsername());
        }

        bundle.putStringArrayList("selectedUsers",selectedUsers);
        intent.putExtras(bundle);
        setResult(RESULT_OK,intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.personal_users_view_all_btn:
                returnResult();
                break;
            case R.id.personal_users_add_user_btn:
                Intent intent = new Intent(PersonalUsersActivity.this,UsersSearchActivity.class);
                startActivityForResult(intent,USER_CREATION_RESULT);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == USER_CREATION_RESULT) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                User user = (User) data.getExtras().get("user");
                new UpdateUserPosts(this).callRequest(user.getUsername());
                personalUsersAdapter.addUser(user);
            }
        }
    }
}
