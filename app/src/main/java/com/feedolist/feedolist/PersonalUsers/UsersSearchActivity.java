package com.feedolist.feedolist.PersonalUsers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.feedolist.feedolist.ServerRequests.GetUsers;
import com.feedolist.feedolist.R;
import com.feedolist.feedolist.ServerRequests.User;
import com.feedolist.feedolist.UsersSearch.SearchedUsersAdapter;

import java.util.ArrayList;

public class UsersSearchActivity extends AppCompatActivity implements GetUsers.IRequest, SearchedUsersAdapter.OnClickListener {

    private EditText searchUsername;

    private SearchedUsersAdapter searchedUsersAdapter;
    private RecyclerView userSelectionRecyclerView;

    private GetUsers getUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getUsers = new GetUsers(this);
        getUsers.setOnRequestResponse(this);
        startUserInterface();
    }

    private void startUserInterface() {
        setContentView(R.layout.personal_user_selection_activity);
        searchUsername = findViewById(R.id.user_selection_username_to_search);
        searchUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 1) handleTextChanged(s.toString());
            }
        });

        userSelectionRecyclerView = findViewById(R.id.users_selection_recycler_view);
        userSelectionRecyclerView.setHasFixedSize(true);
        userSelectionRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        searchedUsersAdapter = new SearchedUsersAdapter(this);
        searchedUsersAdapter.setOnClickListener(this);
        userSelectionRecyclerView.setAdapter(searchedUsersAdapter);


    }

    private void handleTextChanged(String changedText) {
        if(changedText.length() > 2) getUsers.callRequest(changedText);
    }

    private void returnResult(User user) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putSerializable("user", user);
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();
    }


    @Override
    public void onClick(User user) {
        returnResult(user);
    }

    @Override
    public void onRequestResponse(ArrayList<User> users) {
        searchedUsersAdapter.setSearchedUserArrayList(users);
    }
}
