package com.feedolist.feedolist.PersonalUsers;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.feedolist.feedolist.FirebaseDatabase.FirebaseManager;
import com.feedolist.feedolist.PersonalLists.PersonalList;
import com.feedolist.feedolist.R;
import com.feedolist.feedolist.ServerRequests.User;

import java.util.ArrayList;

public class PersonalUsersAdapter extends RecyclerView.Adapter<PersonalUsersViewHolder> implements PersonalUsersViewHolder.OnClickListener, FirebaseManager.IFirebaseRequest{

    private PersonalList personalList;
    private Context context;

    private FirebaseManager firebaseManager = null;

    public PersonalUsersAdapter(PersonalList personalList, Context context){
        this.personalList = personalList;
        this.context = context;

        firebaseManager = new FirebaseManager(context);
        firebaseManager.setOnFirebaseResponse(this);
        firebaseManager.getUserLists();
    }

    @Override
    public PersonalUsersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.personal_user_item_layout,parent,false);
        PersonalUsersViewHolder viewHolder = new PersonalUsersViewHolder(view);
        viewHolder.setOnClickListener(this);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PersonalUsersViewHolder holder, int position) {
        holder.setPersonalUser(personalList.getUsersList().get(position),context);
    }

    @Override
    public int getItemCount() {
        return personalList.getUsersList().size();
    }

    public PersonalList getPersonalList() {
        return personalList;
    }

    public void addUser(User user){
        personalList.getUsersList().add(user);
        firebaseManager.updateListUsers(personalList.getKey(), personalList.getUsersList());
        firebaseManager.getUserLists();
    }

    public void deleteUser(final User personalUser){

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Delete: " + personalUser.getName() + "\nAre you sure?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                personalList.getUsersList().remove(personalUser);
                firebaseManager.updateListUsers(personalList.getKey(), personalList.getUsersList());
                firebaseManager.getUserLists();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    @Override
    public void onFirebaseResponse(ArrayList<Object> list, FirebaseManager.ResponseType responseType) {
        for (PersonalList personalList: (ArrayList<PersonalList>)(ArrayList<?>)(list)) {
            if(this.personalList.getKey().equals(personalList.getKey())){
                this.personalList = personalList;
                notifyDataSetChanged();
                break;
            }
        }
    }

    @Override
    public void onClick(User user, PersonalUsersViewHolder.ClickType clickType) {
        switch (clickType){
            case USER_CLICKED:
                break;
            case DELETE_CLICKED:
                deleteUser(user);
                break;
        }
    }
}
