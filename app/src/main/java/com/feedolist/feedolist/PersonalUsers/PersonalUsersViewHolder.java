package com.feedolist.feedolist.PersonalUsers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.feedolist.feedolist.R;
import com.feedolist.feedolist.ServerRequests.User;
import com.squareup.picasso.Picasso;

public class PersonalUsersViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private ImageView profilePicture;
    private TextView userFullName;
    private TextView username;
    private ImageButton deleteButton;

    private User user;

    public enum ClickType{
        USER_CLICKED,
        DELETE_CLICKED
    }

    public PersonalUsersViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        profilePicture = itemView.findViewById(R.id.personal_user_item_profile_picture);
        userFullName = itemView.findViewById(R.id.personal_user_item_full_name);
        username = itemView.findViewById(R.id.personal_user_item_username);

        deleteButton = itemView.findViewById(R.id.personal_user_item_delete_btn);
        deleteButton.setOnClickListener(this);

    }

    public void setPersonalUser(User user, Context context){
        this.user = user;
        Picasso.get().load(user.getPicture()).into(profilePicture);
        this.userFullName.setText(user.getName());
        this.username.setText("@" + user.getUsername());
    }

    private OnClickListener onClickListener;

    public interface OnClickListener{
        void onClick(User user,ClickType clickType);
    }

    public void setOnClickListener(OnClickListener onClickListener){
        this.onClickListener = onClickListener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.personal_user_item_delete_btn:
                onClickListener.onClick(user,ClickType.DELETE_CLICKED);
                break;
            default:
                onClickListener.onClick(user,ClickType.USER_CLICKED);
                break;
        }
    }
}
