package com.feedolist.feedolist.UsersSearch;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.feedolist.feedolist.R;
import com.feedolist.feedolist.ServerRequests.User;
import com.squareup.picasso.Picasso;

public class SearchedUserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView fullname_tv;
    private TextView username_tv;
    private ImageView userProfilePicture;

    private User user = null;


    public SearchedUserViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        fullname_tv = itemView.findViewById(R.id.searched_fullname);
        username_tv = itemView.findViewById(R.id.searched_username);
        userProfilePicture= itemView.findViewById(R.id.searched_user_profile_picture);

    }

    public void setSearchedUser(User user, Context context) {
        this.user = user;
        Picasso.get().load(user.getPicture()).into(userProfilePicture);
        fullname_tv.setText(user.getName());
        username_tv.setText("@"+user.getUsername());
    }

    private OnClickListener onClickListener;

    public interface OnClickListener{
        void onClick(User searchedUser);
    }

    public void setOnClickListener(OnClickListener onClickListener){
        this.onClickListener = onClickListener;
    }

    @Override
    public void onClick(View v) {
        onClickListener.onClick(user);
    }
}
