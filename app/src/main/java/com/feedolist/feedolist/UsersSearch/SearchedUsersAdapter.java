package com.feedolist.feedolist.UsersSearch;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.feedolist.feedolist.R;
import com.feedolist.feedolist.ServerRequests.User;

import java.util.ArrayList;

public class SearchedUsersAdapter extends RecyclerView.Adapter<SearchedUserViewHolder> implements SearchedUserViewHolder.OnClickListener{

    private ArrayList<User> users  =new ArrayList<>();
    private Context context;

    public SearchedUsersAdapter(Context context){
        this.context = context;
    }

    @Override
    public SearchedUserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.searched_user_view_holder,parent,false);
        SearchedUserViewHolder viewHolder = new SearchedUserViewHolder(view);
        viewHolder.setOnClickListener(this);
        return viewHolder;
    }

    public void setSearchedUserArrayList(ArrayList<User> users){
        if(users != null) {
            this.users = users;
            notifyDataSetChanged();
        }
    }

    @Override
    public void onBindViewHolder(SearchedUserViewHolder holder, int position) {
        holder.setSearchedUser(users.get(position),context);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    private OnClickListener onClickListener;

    public interface OnClickListener{
        void onClick(User user);
    }

    public void setOnClickListener(OnClickListener onClickListener){
        this.onClickListener = onClickListener;
    }

    @Override
    public void onClick(User user) {
        onClickListener.onClick(user);
    }
}
