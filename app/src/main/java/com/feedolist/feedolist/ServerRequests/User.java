package com.feedolist.feedolist.ServerRequests;

import java.io.Serializable;

public class User implements Serializable{
    private int id;
    private String username;
    private String name;
    private String picture;

    public User(){}

    public User(int id, String username, String name, String picture) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.picture = picture;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public String getPicture() {
        return picture;
    }
}
