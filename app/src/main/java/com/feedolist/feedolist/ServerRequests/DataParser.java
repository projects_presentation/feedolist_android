package com.feedolist.feedolist.ServerRequests;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DataParser {

    public static ArrayList<User> ParseUsers(JSONObject users){
        ArrayList<User> usersList = new ArrayList<>();

        try {
            JSONArray jsonArray = users.getJSONArray("users");

            for(int i=0;i<jsonArray.length();++i){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int id = jsonObject.getInt("id");
                String username = jsonObject.getString("username");
                String name = jsonObject.getString("name");
                String picture = jsonObject.getString("picture");
                usersList.add(new User(id,username,name,picture));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return usersList;
    }

    public static User ParseUserFromPosts(JSONObject posts){
        User user = null;
        try {
            JSONObject jsonObject = posts.getJSONObject("user");
            int id = jsonObject.getInt("id");
            String username = jsonObject.getString("username");
            String name = jsonObject.getString("name");
            String picture = jsonObject.getString("picture");
            user = new User(id,username,name,picture);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return user;
    }

    public static ArrayList<Post> ParsePosts(JSONObject jsonObject){

        ArrayList<Post> posts = new ArrayList<>();
        JSONArray results = null;
        try {
            results = jsonObject.getJSONArray("posts");
        } catch (JSONException e) {
            e.printStackTrace();
            return posts;
        }

        for(int i=0; i< results.length();++i){
            JSONObject post = null;
            try {
                post = results.getJSONObject(i);
                int id = post.getInt("id");
                long userId = post.getLong("userId");
                String username = post.getString("username");
                String text = post.getString("text");
                int likes = post.getInt("likes");
                Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(post.getString("date"));
                String imageUrl = post.getString("imageUrl");
                int height = post.getInt("height");
                int width = post.getInt("width");
                posts.add(new Post(id,userId,username,text,likes,date,imageUrl,height,width));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return posts;
    }
}
