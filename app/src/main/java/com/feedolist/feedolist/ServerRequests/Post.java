package com.feedolist.feedolist.ServerRequests;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import java.util.Comparator;
import java.util.Date;

public class Post implements Comparable<Post> {
    private int id;
    private long userId;
    private String username;
    private String text;
    private int likes;
    private Date date;
    private String imageUrl;
    private int height;
    private int width;

    public Post(int id, long userId, String username, String text, int likes, Date date, String imageUrl, int height, int width) {
        this.id = id;
        this.userId = userId;
        this.username = username;
        this.text = text;
        this.likes = likes;
        this.date = date;
        this.imageUrl = imageUrl;
        this.height = height;
        this.width = width;
    }

    public int getId() {
        return id;
    }

    public long getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getText() {
        return text;
    }

    public int getLikes() {
        return likes;
    }

    public Date getDate() {
        return date;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    @Override
    public int compareTo(@NonNull Post post) {
        return post.getDate().compareTo(getDate());
    }
}
