package com.feedolist.feedolist.ServerRequests;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.feedolist.feedolist.CustomSharedPreferences;
import com.feedolist.feedolist.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetUsers implements Response.Listener, Response.ErrorListener{
    private Context context;

    private IRequest iRequest;

    public interface IRequest {
        void onRequestResponse(ArrayList<User> user);
    }

    public void setOnRequestResponse(IRequest iRequest) {
        this.iRequest = iRequest;
    }

    public GetUsers(Context context) {
        this.context = context;
        this.iRequest = null;
    }

    public void callRequest(String username) {

        String serverAddress = CustomSharedPreferences.getString("SERVER_ADDRESS",context);
        String url = serverAddress + "mobile/users/get/?username=" + username;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, this, this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MainActivity.RequestQueue.add(stringRequest);
    }

    @Override
    public void onResponse(Object response) {

        ArrayList<User> users = new ArrayList<>();
        try {
            users = DataParser.ParseUsers(new JSONObject(response.toString()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        iRequest.onRequestResponse(users);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        iRequest.onRequestResponse(null);
        error.printStackTrace();
    }
}
