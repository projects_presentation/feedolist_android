package com.feedolist.feedolist.ServerRequests;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.feedolist.feedolist.CustomSharedPreferences;
import com.feedolist.feedolist.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class GetPosts implements Response.Listener, Response.ErrorListener {

    private Context context;

    private IRequest iRequest;

    public interface IRequest {
        void onRequestResponse(User user, ArrayList<Post> posts);
    }

    public void setOnRequestResponse(IRequest iRequest) {
        this.iRequest = iRequest;
    }

    public GetPosts(Context context) {
        this.context = context;
        this.iRequest = null;
    }

    public void callRequest(String username) {

        new UpdateUserPosts(context).callRequest(username);

        String serverAddress = CustomSharedPreferences.getString("SERVER_ADDRESS",context);
        String url = serverAddress + "mobile/posts/?username=" + username;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, this, this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MainActivity.RequestQueue.add(stringRequest);
    }

    @SuppressWarnings("Check it later!")
    @Override
    public void onResponse(Object response) {

        ArrayList<Post> posts = new ArrayList<>();
        User user = null;
        try {
            user = DataParser.ParseUserFromPosts(new JSONObject(response.toString()));
            posts = DataParser.ParsePosts(new JSONObject(response.toString()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        iRequest.onRequestResponse(user, posts);
        if(posts.size() == 0){
            if(user != null) Toast.makeText(context,"The user: " + user.getName() + " account is private", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        iRequest.onRequestResponse(null,null);
        error.printStackTrace();
    }

}
