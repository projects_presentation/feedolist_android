package com.feedolist.feedolist.ServerRequests;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.feedolist.feedolist.CustomSharedPreferences;
import com.feedolist.feedolist.MainActivity;

public class UpdateUserPosts implements Response.Listener, Response.ErrorListener{
    private Context context;

    public UpdateUserPosts(Context context) {
        this.context = context;
    }

    public void callRequest(String username) {

        String serverAddress = CustomSharedPreferences.getString("SERVER_ADDRESS",context);
        String url = serverAddress + "mobile/posts/?username=" + username;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, this, this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MainActivity.RequestQueue.add(stringRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(Object response) {

    }
}
