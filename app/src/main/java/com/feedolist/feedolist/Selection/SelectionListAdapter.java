package com.feedolist.feedolist.Selection;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.feedolist.feedolist.R;

import java.util.ArrayList;

public class SelectionListAdapter extends RecyclerView.Adapter<ViewHolder> implements CheckBox.OnCheckedChangeListener, ViewHolder.OnItemClicked, ViewHolder.OnCheckedItemChanged {

    private ArrayList<SelectionListItem> mSelectionListItems = new ArrayList<>();
    private ArrayList<String> mSelectedUsersList = new ArrayList<>();

    public enum SelectionType{
        Middle,
        Final
    }

    private SelectionType mSelectionType;
    private Context context;

    public SelectionListAdapter(ArrayList<SelectionListItem> selectionListItems, SelectionType selectionType, Context context){
        mSelectionListItems = selectionListItems;
        mSelectionType = selectionType;
        this.context = context;
    }

    //region OnClick Interfaces
    private OnItemClicked mOnItemClick;

    public interface OnItemClicked{
        void onItemClicked(SelectionListItem selectionListItem);
    }

    public void setOnItemClicked(OnItemClicked onItemClicked){
        this.mOnItemClick = onItemClicked;
    }

    @Override
    public void onItemClicked(SelectionListItem selectionListItem) {
        mOnItemClick.onItemClicked(selectionListItem);
    }

    @Override
    public void onCheckedItemChanged(String username, Boolean checked) {
        if(checked){
            mSelectedUsersList.add(username);
        }
        else {
            mSelectedUsersList.remove(username);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

    }
    //endregion

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.selection_list_row_card_view,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.setOnItemClicked(this);
        viewHolder.setOnCheckedItemChanged(this);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SelectionListItem selectionListItem = mSelectionListItems.get(position);
        switch (mSelectionType){
            case Middle:
                holder.hideItems();
                holder.setTopText(selectionListItem.getTopText());
                break;
            case Final:
                holder.setTopText(selectionListItem.getTopText());
                holder.setBottomText(selectionListItem.getBottomText());
                break;
        }
        holder.setBitmap(selectionListItem.getBitmapSrc(),context);
        holder.setSelectionListItem(selectionListItem);

    }

    @Override
    public int getItemCount() {
        return mSelectionListItems.size();
    }

    public ArrayList<String> getSelectedUsers(){
        return mSelectedUsersList;
    }


}
