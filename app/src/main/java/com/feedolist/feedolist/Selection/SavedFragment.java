package com.feedolist.feedolist.Selection;

import java.util.ArrayList;

public class SavedFragment {

    public String listName;
    public ArrayList<String> usernames;

    public SavedFragment(String listName, ArrayList<String> usernames){
        this.listName = listName;
        this.usernames = usernames;
    }

}
