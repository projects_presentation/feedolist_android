package com.feedolist.feedolist.Selection;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.feedolist.feedolist.R;
import com.squareup.picasso.Picasso;

public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, CheckBox.OnCheckedChangeListener {

    private CheckBox mCheckBox;
    private ImageView mImageView;
    private TextView mTopText;
    private TextView mBottomText;

    private SelectionListItem mSelectionListItem;

    //region OnClick Interfaces
    private OnItemClicked mOnItemClick;
    private OnCheckedItemChanged onCheckedItemChanged;

    public interface OnItemClicked{
        void onItemClicked(SelectionListItem selectionListItem);
    }
    public void setOnItemClicked(OnItemClicked onItemClicked){
        this.mOnItemClick = onItemClicked;
    }

    public interface OnCheckedItemChanged {
        void onCheckedItemChanged(String username, Boolean checked);
    }

    public void setOnCheckedItemChanged(OnCheckedItemChanged onCheckChanged){
        this.onCheckedItemChanged = onCheckChanged;
    }
    //endregion

    public ViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        mCheckBox = (CheckBox) itemView.findViewById(R.id.selection_list_checked);
        mCheckBox.setOnCheckedChangeListener(this);
        mImageView = (ImageView) itemView.findViewById(R.id.selection_list_image_view);
        mTopText = (TextView) itemView.findViewById(R.id.selection_list_user_full_name);
        mBottomText = (TextView) itemView.findViewById(R.id.selection_list_username);
    }

    public void setSelectionListItem(SelectionListItem selectionListItem){
        mSelectionListItem = selectionListItem;
    }

    public void hideItems(){
        mCheckBox.setVisibility(View.GONE);
        mBottomText.setVisibility(View.GONE);
    }

    public void setBitmap(String bitmapSrc, Context context){
        Picasso.get().load(bitmapSrc).into(mImageView);
    }

    public void setTopText(String topText){
        mTopText.setText(topText);
    }

    public void setBottomText(String bottomText){
        mBottomText.setText(bottomText);
    }

    @Override
    public void onClick(View view) {
        mOnItemClick.onItemClicked(mSelectionListItem);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        onCheckedItemChanged.onCheckedItemChanged(mSelectionListItem.getBottomText(), b);
    }
}
