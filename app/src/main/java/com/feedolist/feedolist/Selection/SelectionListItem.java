package com.feedolist.feedolist.Selection;

import android.graphics.Bitmap;

import com.feedolist.feedolist.Extras.Utilities;

import java.io.Serializable;

public class SelectionListItem implements Serializable {

    private String key;
    private String bitmapSrc;
    private String topText;
    private String bottomText;

    public SelectionListItem(String key, String bitmapSrc, String topText, String bottomText){
        this.key = key;
        this.bitmapSrc = bitmapSrc;
        this.topText = topText;
        this.bottomText = bottomText;
    }

    public String getKey() {
        return key;
    }

    public String getBitmapSrc() {
        return bitmapSrc;
    }

    public String getTopText() {
        return topText;
    }

    public String getBottomText() {
        return bottomText;
    }
}
