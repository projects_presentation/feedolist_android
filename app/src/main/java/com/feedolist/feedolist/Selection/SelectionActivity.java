package com.feedolist.feedolist.Selection;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.feedolist.feedolist.R;
import com.feedolist.feedolist.FirebaseDatabase.FirebaseManager;

import java.util.ArrayList;

import static com.feedolist.feedolist.MainActivity.SELECTION_LIST_REQUEST;

public class SelectionActivity extends Activity implements FirebaseManager.IFirebaseRequest, View.OnClickListener, SelectionListAdapter.OnItemClicked{

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;

    private FirebaseManager firebaseManager = null;
    private FirebaseManager.ResponseType responseType;

    private SelectionListAdapter selectionListAdapter = null;

    private ArrayList<SelectionListItem> selectionListItems;

    private String selectedText = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selection_activity);
        setupFirebase();
    }

    private void setupFirebase(){
        firebaseManager = new FirebaseManager(this);
        firebaseManager.setOnFirebaseResponse(this);
        getDataToStartActivity();
    }

    private void getDataToStartActivity(){
        Bundle bundle = getIntent().getExtras();
        String selectionText = (String) bundle.get("selectionText");
        responseType = (FirebaseManager.ResponseType) bundle.get("responseType");
        selectionListItems = (ArrayList<SelectionListItem>) bundle.get("list");
        setupUserInterface(selectionText);
    }

    private void setupUserInterface(String selectionText){
        TextView selectionTextTv = (TextView) findViewById(R.id.selection_text);
        selectionTextTv.setText("#" + selectionText);
        recyclerView = (RecyclerView) findViewById(R.id.selection_list);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        Button viewAllButton = (Button) findViewById(R.id.view_all_button);
        viewAllButton.setOnClickListener(this);

        viewAllButton.setVisibility(responseType == FirebaseManager.ResponseType.Users ? View.VISIBLE: View.GONE);

        selectionListAdapter = new SelectionListAdapter(selectionListItems, responseType == FirebaseManager.ResponseType.Users ? SelectionListAdapter.SelectionType.Final: SelectionListAdapter.SelectionType.Middle,this);
        selectionListAdapter.setOnItemClicked(this);
        recyclerView.setAdapter(selectionListAdapter);


    }

    @Override
    public void onFirebaseResponse(ArrayList<Object> list, FirebaseManager.ResponseType responseType) {
        Intent intent = new Intent(SelectionActivity.this,SelectionActivity.class);
        intent.putExtra("selectionText",selectedText);
        intent.putExtra("responseType",responseType);
        intent.putExtra("list",list);
        startActivityForResult(intent, SELECTION_LIST_REQUEST);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.view_all_button:
                if(selectionListAdapter.getSelectedUsers().size() == 0){
                    Toast.makeText(this, "No selected users!", Toast.LENGTH_SHORT).show();
                }
                else {
                    returnResult(selectionListAdapter.getSelectedUsers());
                }
                break;
        }
    }

    private void returnResult(ArrayList<String> selectedUsers){
        TextView selectionTextTv = (TextView) findViewById(R.id.selection_text);
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putString("selectionText",selectionTextTv.getText().toString());
        bundle.putStringArrayList("selectedUsers",selectedUsers);
        intent.putExtras(bundle);
        setResult(RESULT_OK,intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SELECTION_LIST_REQUEST) {
            if (resultCode == RESULT_OK) {
                setResult(resultCode,data);
                finish();
            }
        }
    }

    @Override
    public void onItemClicked(SelectionListItem selectionListItem) {
        selectedText = selectionListItem.getTopText().replace(" ", "").toLowerCase();
        switch (responseType){
            case Types:
                firebaseManager.getTrendingSubTypesList(selectionListItem.getKey());
                break;
            case SubTypes:
                firebaseManager.getTrendingUsersList(selectionListItem.getKey());
                break;
            case Users:
                ArrayList<String> singleSelectedUser = new ArrayList<String>();
                singleSelectedUser.add(selectionListItem.getBottomText());
                returnResult(singleSelectedUser);
                break;
        }
    }

}
