package com.feedolist.feedolist;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.feedolist.feedolist.Posts.ListFragment;
import com.feedolist.feedolist.Extras.ProgressDialogActivity;
import com.feedolist.feedolist.Extras.Utilities;
import com.feedolist.feedolist.Selection.SavedFragment;
import com.feedolist.feedolist.Selection.SelectionActivity;
import com.feedolist.feedolist.FirebaseDatabase.FirebaseManager;
import com.feedolist.feedolist.PersonalLists.PersonalListsActivity;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class MainActivity extends ProgressDialogActivity implements NavigationView.OnNavigationItemSelectedListener, ListFragment.OnFragmentInteractionListener, FirebaseManager.IFirebaseRequest, GoogleApiClient.OnConnectionFailedListener, ListFragment.OnClickListener {

    public static RequestQueue RequestQueue;

    private FirebaseManager firebaseManager = null;
    private ListFragment listFragment = null;

    private DrawerLayout drawer;

    public static final int SELECTION_LIST_REQUEST = 3000;
    public static final int USERS_SELECTION_REQUEST = 3001;
    private static final int RC_SIGN_IN = 4001;


    private FirebaseAuth firebaseAuth;
    private final String TAG = "firebaseAuthWithGoogle";
    private Boolean userLoggedIn = false;
    private GoogleApiClient mGoogleApiClient;
    private MenuItem authButton = null;

    private ArrayList<SavedFragment> savedFragmentArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        RequestQueue = Volley.newRequestQueue(this);
        CustomSharedPreferences.createSharedPreferences(this);
        super.onCreate(savedInstanceState);
        enableStrictMode();
        startUI();
        startAuthentication();

    }

    private void enableStrictMode() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    private void startUI() {

        setContentView(R.layout.main_activity);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = drawer.findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        authButton = navigationView.getMenu().findItem(R.id.auth_button);

        Utilities.requestPermissions(this);

        SetupFirebaseConnection();

    }

    // [START on_start_check_user]
    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        updateUI(firebaseUser);
    }

    public void startAuthentication(){
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id)).requestEmail().requestProfile().build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();
        firebaseAuth = FirebaseAuth.getInstance();
    }

    private void updateUI(FirebaseUser user) {
        NavigationView navigationView = drawer.findViewById(R.id.nav_view);
        TextView userName = navigationView.getHeaderView(0).findViewById(R.id.drawer_user_name);
        TextView userEmail = navigationView.getHeaderView(0).findViewById(R.id.drawer_user_email);
        ImageView userPicture = navigationView.getHeaderView(0).findViewById(R.id.drawer_user_icon);
        MenuItem myListsBtn = navigationView.getMenu().findItem(R.id.my_lists_nd_btn);

        hideProgressDialog();
        if (user != null) {
            userLoggedIn = true;
            authButton.setTitle(getResources().getString(R.string.sign_out));
            myListsBtn.setVisible(true);
            userName.setText(firebaseAuth.getCurrentUser().getDisplayName());
            userEmail.setText(firebaseAuth.getCurrentUser().getEmail());

            Picasso.get().load(firebaseAuth.getCurrentUser().getPhotoUrl()).into(userPicture);
        } else {
            userLoggedIn = false;
            authButton.setTitle(getResources().getString(R.string.sign_in));
            myListsBtn.setVisible(false);
            userName.setText("#feedolist");
            userEmail.setText("feedolist@gmail.com");
            userPicture.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.icon));

        }
    }

    // [START auth_with_google]
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {

        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        showProgressDialog();

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Toast.makeText(MainActivity.this, "You have successfully sign in", Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                    }
                });
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    // [END signin]

    private void signOut() {
        // Firebase sign out
        firebaseAuth.signOut();
        showProgressDialog();

        // Google sign out
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        Toast.makeText(MainActivity.this, "You have successfully sign out", Toast.LENGTH_SHORT).show();
                        updateUI(null);
                    }
                });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, getString(R.string.error_validating_user), Toast.LENGTH_SHORT).show();
    }

    private void SetupFirebaseConnection() {
        firebaseManager = new FirebaseManager(this);
        firebaseManager.setOnFirebaseResponse(this);
        firebaseManager.getTopSelectionList();
    }

    private void updateContentFragment(String listName,ArrayList<String> usernames) {

        if (listFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(listFragment).commitAllowingStateLoss();
        }

        listFragment = ListFragment.newInstance(listName,usernames);
        savedFragmentArrayList.add(new SavedFragment(listName,usernames));
        listFragment.setOnClickListener(this);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mainActivityContent, listFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        switch (item.getItemId()) {
            case R.id.trending_nd_btn:
                firebaseManager.getTrendingTypesList();
                break;
            case R.id.top_selection_nd_btn:
                firebaseManager.getTopSelectionList();
                break;
            case R.id.my_lists_nd_btn:
                Intent intent = new Intent(MainActivity.this, PersonalListsActivity.class);
                startActivityForResult(intent,USERS_SELECTION_REQUEST);
                break;
            case R.id.auth_button:
                if(userLoggedIn) signOut();
                else signIn();
                break;
        }

        return true;

    }

    @Override
    public void onBackPressed() {
        if(!savedFragmentArrayList.isEmpty() && savedFragmentArrayList.size() > 1){
            SavedFragment lastSavedFragment = savedFragmentArrayList.get(savedFragmentArrayList.size() - 1);
            savedFragmentArrayList.remove(lastSavedFragment);
            lastSavedFragment = savedFragmentArrayList.get(savedFragmentArrayList.size() - 1);
            savedFragmentArrayList.remove(lastSavedFragment);
            updateContentFragment(lastSavedFragment.listName,lastSavedFragment.usernames);
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == SELECTION_LIST_REQUEST || requestCode == USERS_SELECTION_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                ArrayList<String> selectedUsersList = data.getExtras().getStringArrayList("selectedUsers");
                updateContentFragment(data.getExtras().getString("selectionText"),selectedUsersList);
            }
        }

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                // Google Sign In failed, update UI appropriately
                // [START_EXCLUDE]
                updateUI(null);
                // [END_EXCLUDE]
            }
        }
    }

    @Override
    public void onFirebaseResponse(ArrayList<Object> list, FirebaseManager.ResponseType responseType) {
        switch (responseType) {
            case Types:
                Intent intent = new Intent(MainActivity.this,SelectionActivity.class);
                intent.putExtra("selectionText",getResources().getString(R.string.categories));
                intent.putExtra("responseType",responseType);
                intent.putExtra("list", (ArrayList<?>) (list));
                startActivityForResult(intent,SELECTION_LIST_REQUEST);
                break;
            case TopSelection:
                updateContentFragment(getResources().getString(R.string.top_selection),(ArrayList<String>) (ArrayList<?>) (list));
                break;
            case Personalities:
                break;
        }
    }


    @Override
    public void onClick(String username) {
        ArrayList<String> users = new ArrayList<>();
        users.add(username.replace("@",""));
        updateContentFragment(username,users);
    }
}
