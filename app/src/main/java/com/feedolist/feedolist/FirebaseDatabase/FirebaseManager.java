package com.feedolist.feedolist.FirebaseDatabase;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.feedolist.feedolist.R;
import com.feedolist.feedolist.Selection.SelectionListItem;
import com.feedolist.feedolist.PersonalLists.PersonalList;
import com.feedolist.feedolist.ServerRequests.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class FirebaseManager{

    public static enum ResponseType{
        Types,
        SubTypes,
        Users,
        TopSelection,
        Personalities,
        UserLists,
    }

    private Context context;
    private ProgressDialog progressDialog;


    public FirebaseManager(Context context){
        this.context = context;
    }

    private IFirebaseRequest iFirebaseRequest = null;

    public interface IFirebaseRequest {
        void onFirebaseResponse(ArrayList<Object> list, ResponseType responseType);
    }

    public void setOnFirebaseResponse(IFirebaseRequest iFirebaseRequest) {
        this.iFirebaseRequest = iFirebaseRequest;
    }

    private void showProgressBar() {
        Handler h = new Handler(Looper.getMainLooper());
        h.post(new Runnable() {
            public void run() {
                progressDialog = new ProgressDialog(context);
                progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
                progressDialog.show();
            }
        });
    }

    public void createList(PersonalList personalList){
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference("users_lists/" + FirebaseAuth.getInstance().getCurrentUser().getUid());
        String key = databaseReference.push().getKey();
        databaseReference.child(key).setValue(personalList);
    }

    public void updateListName(String userListKey, String newListName){
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference("users_lists/" + FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.child(userListKey).child("listName").setValue(newListName);
    }

    public void updateListUsers(String userListKey ,ArrayList<User> usersArrayList){
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference("users_lists/" + FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.child(userListKey).child("usersList").setValue(usersArrayList);
    }

    public void deleteList(PersonalList personalList){
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference("users_lists/" + FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.child(personalList.getKey()).removeValue();
    }

    public void getUserLists(){

        showProgressBar();
        final ArrayList<Object> list = new ArrayList<>();
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference("users_lists/" + FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data: dataSnapshot.getChildren()) {
                    PersonalList personalList = new PersonalList(data.getKey(), data.child("listName").getValue().toString());
                    GenericTypeIndicator<ArrayList<User>> genericTypeIndicator = new GenericTypeIndicator<ArrayList<User>>(){};
                    ArrayList usersList = data.child("usersList").getValue(genericTypeIndicator);
                    if(usersList == null){
                        usersList = new ArrayList<>();
                    }
                    personalList.setUsersList(usersList);
                    list.add(personalList);
                }
                returnResults(list,ResponseType.UserLists);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getTrendingTypesList(){

        new Thread(new Runnable() {
            @Override
            public void run() {
                showProgressBar();
                final ArrayList<Object> list = new ArrayList<>();
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference databaseReference = database.getReference("trending/types");
                databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot data: dataSnapshot.getChildren()) {
                            try {
                                SelectionListItem selectionListItem = new SelectionListItem(data.getKey(),data.child("profilePicture").getValue().toString(),data.child("text").getValue().toString(),null);
                                list.add(selectionListItem);
                            }catch (Exception ex){
                                Log.d("FirebaseManager", ex.getMessage());
                            }
                        }
                        Comparator<SelectionListItem> comparator = new Comparator<SelectionListItem>() {
                            @Override
                            public int compare(SelectionListItem selectionListItem1, SelectionListItem selectionListItem2) {
                                return selectionListItem1.getTopText().compareTo(selectionListItem2.getTopText());
                            }
                        };
                        returnSortedResults(list,comparator,ResponseType.Types);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }).start();
    }

    public void getTrendingSubTypesList(final String key){

        new Thread(new Runnable() {
            @Override
            public void run() {
                showProgressBar();
                final ArrayList<Object> list = new ArrayList<>();
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference databaseReference = database.getReference("trending/subtypes");
                databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot data: dataSnapshot.getChildren()) {
                            if(data.child("typeKey").getValue().toString().equals(key)){
                                try {
                                    SelectionListItem selectionListItem = new SelectionListItem(data.getKey(),data.child("profilePicture").getValue().toString(),data.child("text").getValue().toString(),null);
                                    list.add(selectionListItem);
                                }catch (Exception ex){
                                    Log.d("FirebaseManager", ex.getMessage());
                                }
                            }
                        }
                        Comparator<SelectionListItem> comparator = new Comparator<SelectionListItem>() {
                            @Override
                            public int compare(SelectionListItem selectionListItem1, SelectionListItem selectionListItem2) {
                                return selectionListItem1.getTopText().compareTo(selectionListItem2.getTopText());
                            }
                        };
                        returnSortedResults(list,comparator,ResponseType.SubTypes);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }).start();

    }

    public void getTrendingUsersList(final String key){

        new Thread(new Runnable() {
            @Override
            public void run() {
                showProgressBar();
                final ArrayList<Object> list = new ArrayList<>();
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference databaseReference = database.getReference("trending/users");
                databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot data: dataSnapshot.getChildren()) {
                            try {
                                if(data.child("subtypeKey").getValue().toString().equals(key)){
                                    SelectionListItem selectionListItem = new SelectionListItem(data.getKey(),data.child("profilePicture").getValue().toString(),data.child("userfullname").getValue().toString(),data.child("username").getValue().toString());
                                    list.add(selectionListItem);
                                }
                            }catch (Exception ex){
                                Log.d("FirebaseManager", ex.getMessage());
                            }

                        }
                        Comparator<SelectionListItem> comparator = new Comparator<SelectionListItem>() {
                            @Override
                            public int compare(SelectionListItem selectionListItem1, SelectionListItem selectionListItem2) {
                                return selectionListItem1.getTopText().compareTo(selectionListItem2.getTopText());
                            }
                        };
                        returnSortedResults(list,comparator,ResponseType.Users);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }).start();

    }

    public void getTopSelectionList(){

        new Thread(new Runnable() {
            @Override
            public void run() {
                showProgressBar();
                final ArrayList<Object> list = new ArrayList<>();
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference databaseReference = database.getReference("top_selection");
                databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot data: dataSnapshot.getChildren()) {
                            list.add(data.child("username").getValue().toString());
                        }
                        returnResults(list,ResponseType.TopSelection);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }).start();


    }

    public void returnResults(ArrayList<Object> objectArrayList, ResponseType responseType){
        iFirebaseRequest.onFirebaseResponse(objectArrayList,responseType);
        progressDialog.dismiss();
    }

    private void returnSortedResults(ArrayList<Object> objectArrayList, Comparator comparator, ResponseType responseType){
        Collections.sort(objectArrayList,comparator);
        iFirebaseRequest.onFirebaseResponse(objectArrayList,responseType);
        progressDialog.dismiss();
    }

}
